export interface IAppConfig {
	urls: {
		backend: {
			refreshToken: string;
			login: string;
		},
		frontend: {
			baseUrlToIncludeInEmail: string;
			confirmEmail: string;
			resetPassword: string;
			signIn: string;
			signUp: string;
			forgotPassword: string;
			resendConfirmationEmail: string;
		}
	}
}