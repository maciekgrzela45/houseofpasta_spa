export interface UpdateAccountDetailsRequest {
	firstName: string;
	lastName: string;
	phoneNumber: string;
	isDeleted: boolean;
}
