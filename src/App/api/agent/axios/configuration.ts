import store from 'App/state/store';
import axios from 'axios';
import { errorHandler } from '../errorHandling/errorHandler';

export const baseURL = `https://localhost:44303/api/`;
axios.defaults.baseURL = baseURL;

axios.interceptors.request.use(
	(config) => {
		// token interceptor goes here
		const token = store.getState()?.auth?.tokens?.token;
		if (token) config.headers.Authorization = `Bearer ${token}`;
		return config;
	},
	(error) => {
		return Promise.reject(error);
	}
);

axios.interceptors.response.use(null, errorHandler);
axios.defaults.withCredentials = true;
