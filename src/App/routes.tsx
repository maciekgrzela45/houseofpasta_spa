import React from 'react';
import { Switch, Route, Redirect } from 'react-router';

import ProtectedRoute from './common/components/ProtectedRoute';

import { default as NotFoundPage } from './pages/ResultPages/NotFoundPage/NotFoundPageContainer';
import { default as ForbiddenPage } from './pages/ResultPages/ForbiddenPage/ForbiddenPageContainer';
import { default as InternalServerErrorPage } from './pages/ResultPages/InternalServerErrorPage/components/InternalServerErrorPageContainer'

import { default as HomePage } from './pages/HomePage/HomePageContainer';
import { default as LoginPage } from './pages/LoginPage/LoginPageContainer';
import { default as AdminPage } from './pages/AdminPage/AdminPageContainer';
import { default as RegisterPage } from './pages/RegisterPage/RegisterPageContainer';
import { default as AccountPage } from './pages/AccountPage/AccountPageContainer';

import Role from './types/role';

const Routes: React.FC = () => {
	return (
		<Switch>
			<Route exact path='/' component={HomePage} />
			<Route exact path='/sign-in' component={LoginPage} />
			<Route exact path='/sign-up' component={RegisterPage} />
			<ProtectedRoute
				acceptedRoles={[Role.ADMIN]}
				path='/admin'
				component={AdminPage}
			/>
			<Route
				path='/account'
				component={AccountPage}
			/>
			<Route path='/403' component={ForbiddenPage} />
			<Route path='/404' component={NotFoundPage} />
			<Route path='/500' component={InternalServerErrorPage} />
			<Redirect to='/404' />
		</Switch>
	);
};

export default Routes;
