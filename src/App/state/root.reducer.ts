import { combineReducers } from '@reduxjs/toolkit';
import accountSlice from './account/account.slice';

import adminLogsSlice from './admin/logs/logs.slice';
import adminUsersSlice from './admin/users/users.slice';

import authSlice from './auth/auth.slice';

const rootReducer = combineReducers({
	admin: combineReducers({
		users: adminUsersSlice.reducer,
		logs: adminLogsSlice.reducer
	}),
	auth: authSlice.reducer,
	account: accountSlice.reducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
