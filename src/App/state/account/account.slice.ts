import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { GetAccountDetailsResponse } from "App/api/endpoints/account/responses";
import StatusType from "App/types/requestStatus";
import { accountInitialState, AccountState } from "./account.state";

const { FAILED, SUCCESS, LOADING } = StatusType;

const accountSlice = createSlice({
    name: 'account',
    initialState: accountInitialState,
    reducers: {
        confirmEmailStart: (state: AccountState) => {
            state.status.confirmEmail = LOADING;
        },
        confirmEmailSuccess: (state: AccountState) => {
            state.status.confirmEmail = SUCCESS;
        },
        confirmEmailFailure: (state: AccountState) => {
            state.status.confirmEmail = FAILED;
        },

        forgotPasswordStart: (state: AccountState) => {
            state.status.forgotPassword = LOADING;
        },
        forgotPasswordSuccess: (state: AccountState) => {
            state.status.forgotPassword = SUCCESS;
        },
        forgotPasswordFailure: (state: AccountState) => {
            state.status.forgotPassword = FAILED;
        },

        updateAccountDetailsStart: (state: AccountState) => {
            state.status.updateAccountDetails = LOADING;
        },
        updateAccountDetailsSuccess: (state: AccountState) => {
            state.status.updateAccountDetails = SUCCESS;
        },
        updateAccountDetailsFailure: (state: AccountState) => {
            state.status.updateAccountDetails = FAILED;
        },

        resetPasswordStart: (state: AccountState) => {
            state.status.resetPassword = LOADING;
        },
        resetPasswordSuccess: (state: AccountState) => {
            state.status.resetPassword = SUCCESS;
        },
        resetPasswordFailure: (state: AccountState) => {
            state.status.resetPassword = FAILED;
        },

        deleteAccountStart: (state: AccountState) => {
            state.status.deleteAccount = LOADING;
        },
        deleteAccountSuccess: (state: AccountState) => {
            state.status.deleteAccount = SUCCESS;
        },
        deleteAccountFailure: (state: AccountState) => {
            state.status.deleteAccount = FAILED;
        },

        getAccountDetailsStart: (state: AccountState) => {
            state.status.getAccountDetails = LOADING;
        },
        getAccountDetailsSuccess: (state: AccountState, action: PayloadAction<GetAccountDetailsResponse>) => {
            state.status.getAccountDetails = SUCCESS;
            state.accountDetails = action.payload;
        },
        getAccountDetailsFailure: (state: AccountState) => {
            state.status.getAccountDetails = FAILED;
        },

        resendConfirmationEmailStart: (state: AccountState) => {
            state.status.resendConfirmationEmail = LOADING;
        },
        resendConfirmationEmailSuccess: (state: AccountState) => {
            state.status.resendConfirmationEmail = SUCCESS;
        },
        resendConfirmationEmailFailure: (state: AccountState) => {
            state.status.resendConfirmationEmail = FAILED;
        },

        clearAccountStart: (state: AccountState) => {
            state.accountDetails = null;
            state.status.getAccountDetails = StatusType.INITIAL;
        },

        cleanUpAccountStatusStart: (state: AccountState) => {
            state.status = accountInitialState.status;
        }
    }
})

export default accountSlice;

export const {
    confirmEmailStart,
    confirmEmailSuccess,
    confirmEmailFailure,

    forgotPasswordStart,
    forgotPasswordSuccess,
    forgotPasswordFailure,

    updateAccountDetailsStart,
    updateAccountDetailsSuccess,
    updateAccountDetailsFailure,

    resetPasswordStart,
    resetPasswordSuccess,
    resetPasswordFailure,

    deleteAccountStart,
    deleteAccountSuccess,
    deleteAccountFailure,

    getAccountDetailsStart,
    getAccountDetailsSuccess,
    getAccountDetailsFailure,

    resendConfirmationEmailStart,
    resendConfirmationEmailSuccess,
    resendConfirmationEmailFailure,

    clearAccountStart,

    cleanUpAccountStatusStart
} = accountSlice.actions;