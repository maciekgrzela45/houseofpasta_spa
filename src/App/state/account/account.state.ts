import { GetAccountDetailsResponse } from "App/api/endpoints/account/responses";
import StatusType from "App/types/requestStatus";
const { INITIAL } = StatusType;


export interface AccountState {
    status: {
        confirmEmail: StatusType;
        forgotPassword: StatusType;
        updateAccountDetails: StatusType;
        resetPassword: StatusType;
        deleteAccount: StatusType;
        getAccountDetails: StatusType;
        resendConfirmationEmail: StatusType;
    };
    accountDetails: GetAccountDetailsResponse | null;
}

export const accountInitialState: AccountState = {
    status: {
        confirmEmail: INITIAL,
        forgotPassword: INITIAL,
        updateAccountDetails: INITIAL,
        resetPassword: INITIAL,
        deleteAccount: INITIAL,
        getAccountDetails: INITIAL,
        resendConfirmationEmail: INITIAL,
    },
    accountDetails: null,
}