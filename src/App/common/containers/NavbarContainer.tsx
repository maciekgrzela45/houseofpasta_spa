import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from 'App/state/root.reducer';
import Role from 'App/types/role';
import AdminNavbar from '../components/Navbar/AdminNavbar';
import UserNavbar from '../components/Navbar/UserNavbar';
import NotLoggedInNavbar from '../components/Navbar/NotLoggedInNavbar';

const NavbarContainer: React.FC = () => {
	const accountDetails = useSelector((state: RootState) => state.account.accountDetails);

	if (accountDetails) {
		if (accountDetails.roles.includes(Role.ADMIN)) {
			return <AdminNavbar accountDetails={accountDetails} />;
		} else if (accountDetails.roles.includes(Role.USER)) {
			return <UserNavbar accountDetails={accountDetails} />;
		}
	} else {
		return <NotLoggedInNavbar />;
	}
};

export default NavbarContainer;
