import { Menu, Image } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import logo from '../../assets/logo.png'
const HomeMenuItem = (props) => {
	const { t } = useTranslation('page');
	return (
		<Menu.Item {...props} key='/'>
			<Link to='/'>
				<Image width={59} preview={false} alt={t('Common.NavbarContainer.Home')} src={logo} />
			</Link>
		</Menu.Item>
	);
};

export default HomeMenuItem;
