import { UserAddOutlined, LoginOutlined, TranslationOutlined, CheckOutlined, MenuOutlined } from '@ant-design/icons';
import { Menu } from 'antd';
import i18n, { languages, fullLanguages } from 'i18n';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useLocation } from 'react-router-dom';
import { Default } from '../Responsive/Default';
import { Mobile } from '../Responsive/Mobile';
import HomeMenuItem from './HomeMenuItem';

const NotLoggedInNavbar = () => {
	const { t } = useTranslation('page');
	const location = useLocation();
	const changeLanguage = (values: any) => {
		i18n.changeLanguage(values.key);
	};

	const signUpMenuItem = (isMobile: boolean) => (
		<Menu.Item key='/sign-up' icon={<UserAddOutlined />} className={isMobile ? '' : 'float-right'}>
			<Link to='/sign-up'>{t('Common.NavbarContainer.SignUp')}</Link>
		</Menu.Item>
	);

	const signInMenuItem = (isMobile: boolean) => (
		<Menu.Item key='/sign-in' icon={<LoginOutlined />} className={isMobile ? '' : 'float-right'}>
			<Link to='/sign-in'>{t('Common.NavbarContainer.SignIn')}</Link>
		</Menu.Item>
	);

	const languagesMenuItem = (isMobile: boolean) => (
		<Menu.SubMenu
			key='languages'
			title={isMobile ? t('Common.NavbarContainer.Language') : ''}
			icon={<TranslationOutlined className={isMobile ? '' : 'mr-0'} />}
			className={isMobile ? '' : 'float-right'}
		>
			{languages.map((language) => (
				<Menu.Item
					key={language}
					onClick={changeLanguage}
					icon={i18n.language === language && <CheckOutlined />}
				>
					{fullLanguages.find((lang) => lang.key === language).value}
				</Menu.Item>
			))}
		</Menu.SubMenu>
	);

	return (
		<>
			<Mobile>
				<Menu mode='horizontal' selectedKeys={[location.pathname]}>
					<HomeMenuItem />
					<Menu.SubMenu className='float-right' icon={<MenuOutlined className='mr-0' />}>
						{signUpMenuItem(true)}
						{signInMenuItem(true)}
						{languagesMenuItem(true)}
					</Menu.SubMenu>
				</Menu>
			</Mobile>

			<Default>
				<Menu mode='horizontal' selectedKeys={[location.pathname]}>
					<HomeMenuItem />
					{signUpMenuItem(false)}
					{signInMenuItem(false)}
					{languagesMenuItem(false)}
				</Menu>
			</Default>
		</>
	);
};

export default NotLoggedInNavbar;
