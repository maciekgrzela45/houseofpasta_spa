import { MenuOutlined } from '@ant-design/icons';
import { Menu } from 'antd';
import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Mobile } from '../Responsive/Mobile';
import { Default } from '../Responsive/Default';
import AccountSubMenu from './AccountSubMenu';
import { useTranslation } from 'react-i18next';
import { GetAccountDetailsResponse } from 'App/api/endpoints/account/responses';
import HomeMenuItem from './HomeMenuItem';

interface AdminNavbarProps {
	accountDetails: GetAccountDetailsResponse;
}

const AdminNavbar: React.FC<AdminNavbarProps> = ({ accountDetails }) => {
	const location = useLocation();
	const { t } = useTranslation('page');

	const adminUsersMenuItem = (
		<Menu.Item key='/admin/users'>
			<Link to='/admin/users'>{t('Common.NavbarContainer.Admin')}</Link>
		</Menu.Item>
	);
	return (
		<>
			<Mobile>
				<Menu mode='horizontal' selectedKeys={[location.pathname]}>
					<HomeMenuItem />
					<Menu.SubMenu className='float-right' icon={<MenuOutlined className='mr-0' />}>
						{adminUsersMenuItem}
						<Menu.Divider />
						<AccountSubMenu isMobile={true} accountDetails={accountDetails} />
					</Menu.SubMenu>
				</Menu>
			</Mobile>

			<Default>
				<Menu mode='horizontal' selectedKeys={[location.pathname]}>
					<HomeMenuItem />
					{adminUsersMenuItem}
					<AccountSubMenu isMobile={false} accountDetails={accountDetails} />
				</Menu>
			</Default>
		</>
	);
};

export default AdminNavbar;
