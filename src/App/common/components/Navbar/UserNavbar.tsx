import { MenuOutlined } from '@ant-design/icons';
import { Menu } from 'antd';
import { GetAccountDetailsResponse } from 'App/api/endpoints/account/responses';
import React from 'react';
import { useLocation } from 'react-router-dom';
import { Default } from '../Responsive/Default';
import { Mobile } from '../Responsive/Mobile';
import AccountSubMenu from './AccountSubMenu';
import HomeMenuItem from './HomeMenuItem';

interface UserNavbarProps {
	accountDetails: GetAccountDetailsResponse;
}

const UserNavbar: React.FC<UserNavbarProps> = ({ accountDetails }) => {
	const location = useLocation();
	return (
		<>
			<Mobile>
				<Menu mode='horizontal' selectedKeys={[location.pathname]}>
					<HomeMenuItem />
					<Menu.SubMenu className='float-right' icon={<MenuOutlined className='mr-0' />}>
						<Menu.Divider />
						<AccountSubMenu isMobile={true} accountDetails={accountDetails} />
					</Menu.SubMenu>
				</Menu>
			</Mobile>

			<Default>
				<Menu mode='horizontal' selectedKeys={[location.pathname]}>
					<HomeMenuItem />
					<AccountSubMenu isMobile={false} accountDetails={accountDetails} />
				</Menu>
			</Default>
		</>
	);
};

export default UserNavbar;
