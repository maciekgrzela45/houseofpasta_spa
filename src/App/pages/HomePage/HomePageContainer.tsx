import React from 'react';
import { Image, Row } from 'antd';

import logo from '../../common/assets/logo.png'

const HomePageContainer: React.FC<{}> = () => {
	return (
		<div>
			<Row className="p-5" justify="center">
				<Image src={logo} />
			</Row>
			<Row justify="center">
				<h2>React Template</h2>
			</Row>
			<Row justify="center">
				<h3>Strona główna</h3>
			</Row>
		</div>
	);
};

export default HomePageContainer;
