import React from 'react';
import { Result, Button } from 'antd';
import { RouteComponentProps } from 'react-router';
import { useTranslation } from 'react-i18next';

interface InternalServerErrorPageContainerProps extends RouteComponentProps { }

const InternalServerErrorPageContainer: React.FC<InternalServerErrorPageContainerProps> = ({ history }: InternalServerErrorPageContainerProps) => {
    const buttonGoBackHomeOnClick = () => history.push('/');
    const { t } = useTranslation(['page', 'common']);

    return (
        <Result
            status='500'
            title='500'
            subTitle={t('InternalServerErrorPage.InternalServerErrorPageContainer.WeAreSorry')}
            extra={
                <Button type='primary' onClick={buttonGoBackHomeOnClick}>
                    {t('common:Actions.BackToHome')}
                </Button>
            }
        ></Result>
    );
};

export default InternalServerErrorPageContainer;
