import React from 'react';


import GetLogsContainer from './containers/GetLogsContainer';
import { Route, Switch } from 'react-router';

const AdminPageLogsContainer: React.FC<{}> = () => {
	return (
		<Switch>
			<Route exact path='/admin/logs' component={GetLogsContainer} />
		</Switch>
	);
};

export default AdminPageLogsContainer;
