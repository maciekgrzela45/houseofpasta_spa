import React from 'react';

import { Layout } from 'antd';
import { useMediaQuery } from 'react-responsive';

import { default as AdminNavbar } from './containers/AdminNavbarContainer';
import { default as AdminPageUsers } from './users/AdminPageUsersContainer';
import { default as AdminPageLogs } from './logs/AdminPageLogsContainer';

import './AdminPageContainer.less';

const AdminPageContainer: React.FC<{}> = () => {
	const isMobile = useMediaQuery({ query: '(max-width: 480px)' });

	const Content = (
		<>
			<AdminPageUsers />
			<AdminPageLogs />
		</>
	);
	return (
		<Layout>
			<Layout.Sider className="admin-page-sidebar bg-site" collapsible defaultCollapsed={isMobile} collapsedWidth='40px' width={256}>
				<AdminNavbar />
			</Layout.Sider>
			<Layout.Content className='pt-3 content-layout'>{Content}</Layout.Content>
		</Layout>
	);
};

export default AdminPageContainer;
