import React from 'react';

import GetUsersContainer from './containers/GetUsersContainer';
import UpdateUserContainer from './containers/UpdateUserContainer';
import CreateUserContainer from './containers/CreateUserContainer';
import GetUserContainer from './containers/GetUserContainer';
import { Route, Switch } from 'react-router';

const AdminPageUsersContainer: React.FC<{}> = () => {
	return (
		<Switch>
			<Route exact path='/admin/users' component={GetUsersContainer} />
			<Route
				exact
				path='/admin/users/create'
				component={CreateUserContainer}
			/>
			<Route
				exact
				path='/admin/users/:userId/update'
				component={UpdateUserContainer}
			/>
			<Route path='/admin/users/:userId' component={GetUserContainer} />
		</Switch>
	);
};

export default AdminPageUsersContainer;
