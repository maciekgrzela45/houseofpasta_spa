import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import { Row, Col, Button, Table, Input } from 'antd';
import { PlusOutlined } from '@ant-design/icons';

import { renderTableColumns } from '../utils/UsersTable';
import defaultPageQueryParams from 'App/common/utils/defaultPageQueryParams';
import { getUsers } from 'App/state/admin/users/users.thunk';
import { RootState } from 'App/state/root.reducer';
import { StatusType } from 'App/types/requestStatus';
import { cleanUpUserStatus } from 'App/state/admin/users/users.slice';
import { useTranslation } from 'react-i18next';
import { TablePaginationConfig } from 'antd/lib/table';

const { LOADING } = StatusType;

const GetUsersContainer = () => {
	const dispatch = useDispatch();
	const { t } = useTranslation();

	const users = useSelector((state: RootState) => state.admin.users.users);
	const usersStatus = useSelector((state: RootState) => state.admin.users.status);

	const { pageNumber, pageSize, totalNumberOfItems } = useSelector(
		(state: RootState) => state.admin.users.getUsersParams
	);

	useEffect(() => {
		dispatch(getUsers(defaultPageQueryParams));

		return () => {
			dispatch(cleanUpUserStatus());
		};
	}, [dispatch]);

	const handleTableChange = (pagination, filters, sorter, extra): any => {
		let sortOrder;
		if (sorter.order === 'ascend') {
			sortOrder = 'Asc';
		} else if (sorter.order === 'descend') {
			sortOrder = 'Desc';
		}

		let fieldToSort;
		if (sorter.field === 'lastName') {
			fieldToSort = 'LastName';
		} else if (sorter.field === 'email') {
			fieldToSort = 'Email';
		}

		let orderBy;
		if (sortOrder && fieldToSort) {
			orderBy = `${fieldToSort}${sortOrder}`;
		}

		dispatch(
			getUsers({
				...defaultPageQueryParams,
				pageNumber: pagination.current || 1,
				pageSize: pagination.pageSize || 10,
				query: '',
				orderBy
			})
		);
	};

	const paginationConfig: TablePaginationConfig = {
		pageSize,
		current: pageNumber,
		total: totalNumberOfItems,
		showSizeChanger: true
	};

	return (
		<>
			<Row>
				<Col span={24}>
					<Link to='/admin/users/create'>
						<Button icon={<PlusOutlined />}>{t('AdminPage.GetUsersContainer.NewUserButton')}</Button>
					</Link>
				</Col>
			</Row>
			<Row>
				<Col span={24}>
					<Input
						allowClear
						onChange={(val) =>
							dispatch(
								getUsers({
									...defaultPageQueryParams,
									query: val.currentTarget.value
								})
							)
						}
					/>
				</Col>
				<Col span={24}>
				<Table
						tableLayout='auto'
						pagination={paginationConfig}
						onChange={handleTableChange}
						loading={usersStatus.getUsers === LOADING}
						columns={renderTableColumns(users, dispatch, t)}
						dataSource={users}
						rowKey='id'
						scroll={{ x: 'fit-content'}}
					/>
				</Col>
			</Row>
		</>
	);
};

export default GetUsersContainer;
