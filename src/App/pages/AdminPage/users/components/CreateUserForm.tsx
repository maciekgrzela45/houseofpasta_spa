import React from 'react';
import { Form, Input, Select, Button } from 'antd';
import { CreateUserRequest } from 'App/api/endpoints/admin/requests';
import { useTranslation } from 'react-i18next';
import { createUserFormRules } from '../utils/usersFormRules';
import { languages } from 'i18n';

interface CreateUserFormProps {
	onFinish: (values: CreateUserRequest) => void;
	initialValues?: CreateUserRequest;
	loading: boolean;
}

const CreateUserForm: React.FC<CreateUserFormProps> = ({ initialValues, loading, onFinish }) => {
	const { t } = useTranslation(['form', 'common']);

	const layout = {
		labelCol: {
			xs: {
				span: 24
			},
			md: {
				span: 3,
				offset: 4,
			},
		},
		wrapperCol: {
			xs: {
				span: 24
			},
			md: {
				span: 10,
			},
		}
	};

	const tailLayout = {
		wrapperCol: {
			xs: {
				span: 24,
			},
			md: {
				span: 10,
				offset: 7
			}
		}
	};

	return (
		<Form {...layout} labelAlign="right" className="px-3" initialValues={initialValues} onFinish={onFinish}>
			<Form.Item
				messageVariables={{ arg: t('form:Common.User.Labels.Email') }}
				label={t('form:Common.User.Labels.Email')}
				name='email'
				rules={createUserFormRules.email()}
			>
				<Input placeholder={t('form:Common.User.Placeholders.Email')} />
			</Form.Item>

			<Form.Item
				label={t('form:Common.User.Labels.Password')}
				messageVariables={{ arg: t('form:Common.User.Labels.Password') }}
				name='password'
				rules={createUserFormRules.password()}
			>
				<Input type='password' placeholder={t('form:Common.User.Placeholders.Password')} />
			</Form.Item>

			<Form.Item
				label={t('form:Common.User.Labels.FirstName')}
				messageVariables={{ arg: t('form:Common.User.Labels.FirstName') }}
				name='firstName'
				rules={createUserFormRules.firstName()}
			>
				<Input placeholder={t('form:Common.User.Placeholders.FirstName')} />
			</Form.Item>

			<Form.Item
				label={t('form:Common.User.Labels.LastName')}
				messageVariables={{ arg: t('form:Common.User.Labels.LastName') }}
				name='lastName'
				rules={createUserFormRules.lastName()}
			>
				<Input placeholder={t('form:Common.User.Placeholders.LastName')} />
			</Form.Item>

			<Form.Item
				name='roles'
				label={t('form:Common.User.Labels.Roles')}
				messageVariables={{ arg: t('form:Common.User.Labels.Roles') }}
				rules={createUserFormRules.roleName()}
			>
				<Select mode='multiple' placeholder={t('form:Common.User.Placeholders.SelectRoles')}>
					<Select.Option value='User'>{t('common:Roles.User')}</Select.Option>
					<Select.Option value='Administrator'>{t('common:Roles.Administrator')}</Select.Option>
				</Select>
			</Form.Item>

			<Form.Item
				name='language'
				label={t('form:Common.User.Labels.EmailLanguage')}
				messageVariables={{ arg: t('form:Common.User.Labels.EmailLanguage') }}
				rules={createUserFormRules.emailLanguage()}
			>
				<Select placeholder={t('form:Common.User.Placeholders.EmailLanguage')}>
					{languages.map((language) => (
						<Select.Option key={language} value={language}>{language}</Select.Option>
					))}
				</Select>
			</Form.Item>

			<Form.Item {...tailLayout}>
				<Button block loading={loading} type='primary' htmlType='submit'>
					{t('common:Actions.Create')}
				</Button>
			</Form.Item>
		</Form>
	);
};

export default CreateUserForm;
