import React from 'react';
import { Form, Input, Button } from 'antd';
import { FormProps } from 'antd/lib/form/Form';
import { resetPasswordFormRules } from '../utils/resetPasswordFormRules';
import { useTranslation } from 'react-i18next';

interface ResetPasswordFormProps extends FormProps {
    loading: boolean;
}

const ResetPasswordForm: React.FC<ResetPasswordFormProps> = (props: ResetPasswordFormProps) => {
    const { t } = useTranslation(['form', 'common']);
    const { loading, ...rest } = props;

    return (
        <Form {...rest}>
            <Form.Item name='newPassword' messageVariables={{ arg: t('form:ResetPassword.Labels.NewPassword') }} rules={resetPasswordFormRules.newPassword()}>
                <Input
                    type='password'
                    placeholder={t('form:ResetPassword.Placeholders.NewPassword')}
                />
            </Form.Item>
            <Form.Item name='confirmNewPassword' messageVariables={{ arg: t('form:ResetPassword.Labels.ConfirmNewPassword') }} rules={resetPasswordFormRules.newConfirmPassword()}>
                <Input
                    type='password'
                    placeholder={t('form:ResetPassword.Placeholders.ConfirmNewPassword')}
                />
            </Form.Item>
            <Form.Item>
                <Button loading={loading} type='primary' htmlType='submit' size='large'>
                    {t('common:Actions.Submit')}
                </Button>
            </Form.Item>
        </Form>
    );
};

export default ResetPasswordForm;
