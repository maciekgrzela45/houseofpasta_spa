import { Rule } from 'antd/lib/form';
import i18n from 'i18n';

export const resetPasswordFormRules = {
    newPassword: (): Rule[] => [
        {
            pattern: RegExp('\\d'),
            message: i18n.t('form:ValidationMessages.Custom.MustContainNumber', { arg: i18n.t('form:ResetPassword.Labels.NewPassword') })
        },
        {
            pattern: RegExp('[*@!#%&()^~{}]+'),
            message: i18n.t('form:ValidationMessages.Custom.MustContainSpecial', { arg: i18n.t('form:ResetPassword.Labels.NewPassword') })
        },
        {
            pattern: RegExp('(?=.*[a-z])'),
            message: i18n.t('form:ValidationMessages.Custom.MustContainLowercase', { arg: i18n.t('form:ResetPassword.Labels.NewPassword') })
        },
        {
            pattern: RegExp('(?=.*[A-Z])'),
            message: i18n.t('form:ValidationMessages.Custom.MustContainUppercase', { arg: i18n.t('form:ResetPassword.Labels.NewPassword') })
        },
        {
            min: 6
        },
        {
            required: true,
            max: 100
        }
    ],

    newConfirmPassword: (): Rule[] => [
        {
            required: true
        },
        ({ getFieldValue }) => ({
            validator(rule, value) {
                if (!value || getFieldValue('newPassword') === value) {
                    return Promise.resolve();
                }

                const arg = [i18n.t('form:ResetPassword.Labels.NewPassword'), i18n.t('form:ResetPassword.Labels.ConfirmNewPassword')]
                return Promise.reject(i18n.t('form:ValidationMessages.Custom.PasswordMismatch', { arg }));
            }
        })
    ]

};