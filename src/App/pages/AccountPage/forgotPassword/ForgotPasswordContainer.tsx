import { Col, notification, PageHeader, Row } from 'antd';
import { forgotPassword } from 'App/state/account/account.thunk';
import { RootState } from 'App/state/root.reducer';
import StatusType from 'App/types/requestStatus';
import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Store } from 'antd/lib/form/interface';
import { ForgotPasswordRequest } from 'App/api/endpoints/account/requests';
import ForgotPasswordForm from './components/ForgotPasswordForm';
import { cleanUpAccountStatusStart } from 'App/state/account/account.slice';


interface ForgotPasswordContainerProps extends RouteComponentProps { }

const ForgotPasswordContainer: React.FC<ForgotPasswordContainerProps> = ({ history }: ForgotPasswordContainerProps) => {
    type FinishFormType = (values: Store) => void;

    const dispatch = useDispatch();
    const status = useSelector((state: RootState) => state.account.status.forgotPassword);
    const { t } = useTranslation('page');

    useEffect(() => {
        return () =>
            dispatch(cleanUpAccountStatusStart())
    }, [dispatch])

    const forgotPasswordHandler: FinishFormType = (values: ForgotPasswordRequest) => {
        let handleSuccess: () => void = () => {
            history.push('/');
            notification.success({
                message: t('common:Success.Success'),
                description: t('AccountPage.ForgotPasswordContainer.ForgotPasswordEmailSuccess')
            })
        };

        dispatch(forgotPassword(values, handleSuccess));
    };

    return (
        <div className='forgotPasswordContainer'>
            <Row align='middle' justify='center'>
                <Col xs={22} md={14} xl={10} xxl={8}>
                    <br />
                    <PageHeader title={t('AccountPage.ForgotPasswordContainer.PageHeaderTitle')} />
                    <ForgotPasswordForm
                        className='forgotPassword-form'
                        name='forgotPasswordForm'
                        size='large'
                        onFinish={forgotPasswordHandler}
                        autoComplete='off'
                        loading={status === StatusType.LOADING}
                    />
                </Col>
            </Row>
        </div>
    )
}

export default ForgotPasswordContainer;
