import { Rule } from 'antd/lib/form';

export const forgotPasswordFormRules = {
    email: (): Rule[] => [{ required: true, type: 'email', max: 255 }],
}
