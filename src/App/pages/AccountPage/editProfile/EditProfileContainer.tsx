import { ExclamationCircleOutlined } from '@ant-design/icons';
import { Button, Col, Divider, notification, PageHeader, Popconfirm, Result, Row } from 'antd';
import { UpdateAccountDetailsRequest } from 'App/api/endpoints/account/requests/updateAccountDetailsRequest';
import LoadingScreen from 'App/common/components/LoadingScreen';
import { logout } from 'App/state/auth/auth.thunk';
import { deleteAccount, getAccountDetails, updateAccountDetails } from 'App/state/account/account.thunk';
import { RootState } from 'App/state/root.reducer';
import StatusType from 'App/types/requestStatus';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import EditProfileForm from './components/EditProfileForm';
import { useHistory } from 'react-router';

const EditProfileContainer: React.FC = () => {
	const dispatch = useDispatch();
	const { t } = useTranslation(['page', 'common']);
	const history = useHistory();

	const accountDetails = useSelector((state: RootState) => state.account.accountDetails);
	const updateAccountStatus = useSelector((state: RootState) => state.account.status.updateAccountDetails);
	const getAccountDetailsStatus = useSelector((state: RootState) => state.account.status.getAccountDetails);
	const deleteAccountStatus = useSelector((state: RootState) => state.account.status.deleteAccount)

	useEffect(() => {
		if (!accountDetails)
			dispatch(getAccountDetails());
	}, [dispatch, accountDetails]);

	const onFinish = (values: UpdateAccountDetailsRequest) => {
		const onSuccess = () => {
			notification.success({
				message: t('common:Success.Success'),
				description: t('AccountPage.EditProfileContainer.UpdateAccountDetailsSuccess')
			});
		};
		dispatch(updateAccountDetails(values, onSuccess));
	};

	const deleteAccountHandler = () => {
		const onDeleteAccountSuccess = () => {
			notification.success({
				message: t('common:Success.Success'),
				description: t('AccountPage.EditProfileContainer.DeleteAccountSuccess')
			});

			let onLogoutSuccess = () => {
				history.push('/');
			};

			dispatch(logout(onLogoutSuccess));
		};
		dispatch(
			deleteAccount({ ...(accountDetails as UpdateAccountDetailsRequest), isDeleted: true }, onDeleteAccountSuccess)
		);
	};

	if (accountDetails) {
		return (
			<>
				<Row justify='center'>
					<Col span={24}>
						<Row justify="center">
							<Col>
								<PageHeader className="p-3" title={t('AccountPage.EditProfileContainer.PageTitle')} />
							</Col>
						</Row>
						<EditProfileForm
							name='editProfileForm'
							size='large'
							onFinish={onFinish}
							initialValues={accountDetails}
							loading={updateAccountStatus === StatusType.LOADING}
						/>
						<Divider />
					</Col>
				</Row>
				<Row justify='center'>
					<Col>
						<Popconfirm
							title={t('common:Warnings.ActionWillBeIrreversible')}
							icon={<ExclamationCircleOutlined style={{ color: 'red' }} />}
							onConfirm={deleteAccountHandler}
						>
							<Button loading={deleteAccountStatus === StatusType.LOADING} danger>{t('AccountPage.EditProfileForm.DeleteAccount')}</Button>
						</Popconfirm>
					</Col>
				</Row>
			</>
		);
	} else {
		if (getAccountDetailsStatus === StatusType.LOADING) {
			return <LoadingScreen container='fill' />;
		} else {
			return <Result
				status='warning'
				title={t('common:Errors.AnErrorOccured')}
				extra={
					<Button type='primary' onClick={() => history.push('/')}>
						{t('common:Actions.BackToHome')}
					</Button>
				}
			/>
		}
	}
};

export default EditProfileContainer;
