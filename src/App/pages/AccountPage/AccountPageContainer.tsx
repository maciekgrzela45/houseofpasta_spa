import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { default as ConfirmEmail } from './confirmEmail/ConfirmEmailContainer';
import { default as EditProfile } from './editProfile/EditProfileContainer';
import { default as ForgotPassword } from './forgotPassword/ForgotPasswordContainer';
import { default as ResendConfirmationEmail } from './resendConfirmationEmail/ResendConfirmationEmailContainer';
import { default as ResetPassword } from './resetPassword/ResetPasswordContainer';

import ProtectedRoute from 'App/common/components/ProtectedRoute';

import Role from 'App/types/role';

const AccountPageContainer: React.FC<{}> = () => {
    return (
        <Switch>
            <Route exact path='/account/reset-password' component={ResetPassword} />
            <Route exact path='/account/resend-confirmation-email' component={ResendConfirmationEmail} />
            <Route path='/account/confirm-email/:userId/:confirmationCode' component={ConfirmEmail} />
            <Route exact path='/account/forgot-password' component={ForgotPassword} />
            <Route path='/account/reset-password/:userId/:passwordResetCode' component={ResetPassword} />
            <ProtectedRoute
                path='/account/edit-profile'
                component={EditProfile}
                acceptedRoles={[Role.ADMIN, Role.USER]}
            />
        </Switch>
    );
};

export default AccountPageContainer;
