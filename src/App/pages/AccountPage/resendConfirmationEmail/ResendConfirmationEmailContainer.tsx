

import { Col, notification, Row, Typography } from 'antd';
import { resendConfirmationEmail } from 'App/state/account/account.thunk';
import { RootState } from 'App/state/root.reducer';
import StatusType from 'App/types/requestStatus';
import React from 'react'
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Store } from 'antd/lib/form/interface';
import { ResendConfirmationEmailRequest } from 'App/api/endpoints/account/requests';
import ResendConfirmationEmailForm from './components/ResendConfirmationEmailForm';


interface ResendConfirmationEmailContainerProps extends RouteComponentProps { }

const ResendConfirmationEmailContainer: React.FC<ResendConfirmationEmailContainerProps> = ({ history }: ResendConfirmationEmailContainerProps) => {
    type FinishFormType = (values: Store) => void;

    const dispatch = useDispatch();
    const status = useSelector((state: RootState) => state.account.status.resendConfirmationEmail);
    const { t } = useTranslation('page');

    const resendConfirmationEmailHandler: FinishFormType = (values: ResendConfirmationEmailRequest) => {
        let handleSuccess: () => void = () => {
            history.push('/sign-in');
            notification.success({
                message: t('common:Success.Success'),
                description: t('AccountPage.ResendConfirmationEmailContainer.EmailWasResent')
            })
        };

        dispatch(resendConfirmationEmail(values, handleSuccess));
    };

    return (
        <div>
            <Row align='middle' justify='center'>
                <Col xs={22} md={14} xl={10} xxl={8}>
                    <br />
                    <Typography.Title level={4}>{t('AccountPage.ResendConfirmationEmailContainer.PageHeaderTitle')}</Typography.Title>
                    <ResendConfirmationEmailForm
                        name='resendConfirmationEmailForm'
                        size='large'
                        onFinish={resendConfirmationEmailHandler}
                        autoComplete='off'
                        loading={status === StatusType.LOADING}
                    />
                </Col>
            </Row>
        </div>
    )
}

export default ResendConfirmationEmailContainer;
