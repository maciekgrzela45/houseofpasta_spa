import { Rule } from 'antd/lib/form';

export const resendConfirmationEmailFormRules = {
    email: (): Rule[] => [{ required: true, type: 'email', max: 255 }],
};