import React from 'react';
import { Form, Input, Button } from 'antd';
import { FormProps } from 'antd/lib/form/Form';
import { registerFormRules } from '../utils/registerPageFormRules';
import { useTranslation } from 'react-i18next';
import i18n from '../../../../i18n';
import Role from 'App/types/role';

interface RegisterFormProps extends FormProps {
    loading: boolean;
}

const RegisterForm: React.FC<RegisterFormProps> = (props: RegisterFormProps) => {
    const { t } = useTranslation(['page', 'form', 'common']);
    const { loading, ...rest } = props;

    return (
        <Form {...rest}>
            <Form.Item name='firstName' messageVariables={{ arg: t('form:Common.User.Labels.FirstName') }} rules={registerFormRules.firstName()}>
                <Input
                    placeholder={t('form:Common.User.Placeholders.FirstName')}
                />
            </Form.Item>
            <Form.Item name='lastName' messageVariables={{ arg: t('form:Common.User.Labels.LastName') }} rules={registerFormRules.lastName()}>
                <Input
                    placeholder={t('form:Common.User.Placeholders.LastName')}
                />
            </Form.Item>
            <Form.Item name='email' messageVariables={{ arg: t('form:Common.User.Labels.Email') }} rules={registerFormRules.email()}>
                <Input
                    placeholder={t('form:Common.User.Placeholders.Email')}
                />
            </Form.Item>
            <Form.Item name='password' messageVariables={{ arg: t('form:Common.User.Labels.Password') }} rules={registerFormRules.password()}>
                <Input
                    type='password'
                    placeholder={t('form:Common.User.Placeholders.Password')}
                />
            </Form.Item>
            <Form.Item name='confirmPassword' messageVariables={{ arg: t('form:Common.User.Labels.ConfirmPassword') }} rules={registerFormRules.confirmPassword()}>
                <Input
                    type='password'
                    placeholder={t('form:Common.User.Placeholders.ConfirmPassword')}
                />
            </Form.Item>
            <Form.Item name='language' initialValue={i18n.language} hidden>
                <Input hidden />
            </Form.Item>
            <Form.Item name='roles' initialValue={[Role.USER]} hidden>
                <Input hidden />
            </Form.Item>
            <Form.Item>
                <Button loading={loading} type='primary' htmlType='submit' size='large'>
                    {t('common:Actions.SignUp')}
                </Button>
            </Form.Item>
        </Form>
    );
};

export default RegisterForm;
